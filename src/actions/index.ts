import * as newoperation from './newoperation'
import * as newschema from './newschema'
import * as annotate from './annotate'
import * as addprop from './addprop'
import * as addelem from './addelem'
import * as print from './print'
import * as edit from './edit'
import * as rm from './rm'
import * as cd from './cd'
import * as ls from './ls'

export default {
	...newoperation,
	...newschema,
	...annotate,
	...addprop,
	...addelem,
	...print,
	...edit,
	...rm,
	...cd,
	...ls
}
