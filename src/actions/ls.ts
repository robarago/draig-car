import util from 'util'
import chalk from 'chalk'

import { exitOrContinue } from '../main/util'
import { apiValue } from './common'

export function lsAction(args: string[] | string) {
	const ctx = this.context
	const [value, hashKey] = apiValue(ctx, args)
	if (typeof value === 'undefined' || typeof value === 'string')
		console.log(
			chalk`Sorry, element at {yellow ${args}} not found or not an object`
		)
	else
		console.log(
			Object.keys(value).map(k => {
				const o = eval(`ctx.api${hashKey}['${k}']`)
				return k + (util.isArray(o) ? '@' : util.isObject(o) ? ':' : '')
			})
		)
	exitOrContinue(this)
}
