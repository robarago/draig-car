import util from 'util'
import chalk from 'chalk'
import mustache from 'mustache'
import { Context } from 'node:vm'

import { View, OA3Operation, OA3Schema, OA3Object, OA3Tag } from '../types/api'
import { template, printYaml, fromYaml } from '../main/util'

let workSchema: OA3Schema = {}

/* eslint-disable @typescript-eslint/no-explicit-any */
function getParsedTemplate(tpl: string) {
	const parsed = mustache.parse(tpl)
	const r = (e: any) =>
		e[0] === 'name' || e[0] === '&'
			? e[1]
			: e[0] === '#'
			? { [e[1]]: e[4].reduce((i: Array<any>, o: any) => i.concat(r(o)), []) }
			: e[0] === '^'
			? {
					['not' + e[1]]: e[4].reduce(
						(i: Array<any>, o: any) => i.concat(r(o)),
						[]
					)
			  }
			: []
	return parsed.reduce((i, o) => i.concat(r(o)), [])
}

const completers = (api: View): { [id: string]: string[] } => ({
	yesno: ['yes', 'no'],
	path: Object.keys(api.paths),
	tag: api.tags.map((e: OA3Tag) => e.name),
	schema: Object.keys(api.components.schemas),
	bodySchema: Object.keys(api.components.schemas),
	schemaPart: Object.keys(api.components.schemas),
	columnType: ['integer', 'string', 'number', 'boolean'],
	refType: Object.keys(api.components.schemas),
	required:
		'properties' in workSchema
			? Object.keys(workSchema.properties)
			: 'allOf' in workSchema &&
			  workSchema.allOf.find((o: OA3Object) => 'properties' in o)
			? Object.keys(
					(workSchema.allOf.find(o => 'properties' in o) as OA3Object)
						.properties
			  )
			: [],
	format: [
		'int32',
		'int64',
		'float',
		'double',
		'byte',
		'binary',
		'date',
		'date-time',
		'password',
		'email',
		'uuid'
	]
})

/* eslint-disable @typescript-eslint/no-explicit-any */
function completer(repl: any, bar: string[] | string) {
	return (line: string, callback: (err: Error | null, result: any) => void) => {
		const comps = util.isArray(bar)
			? bar
			: completers(repl.context.api)[bar] || []
		const hits = comps.filter((c: string) => c.startsWith(line))
		const ret = [hits && hits.length ? hits : comps, line || '']
		callback(null, ret)
	}
}

/* eslint-disable @typescript-eslint/no-explicit-any */
async function askVarObjects(repl: any, varName: string, vlist: any) {
	const values = {}
	for (const v of vlist) await getVars(repl, values, v)
	return values
}

/* eslint-disable @typescript-eslint/no-explicit-any */
export function question(
	repl: any,
	completions: string[] | string,
	msg: string,
	defvalue?: string
): Promise<string> {
	return new Promise(resolve => {
		const _comp = repl.completer
		repl.completer = completer(repl, completions)
		if (defvalue) repl.history.unshift(defvalue)
		repl.question(msg, (r: string) => {
			repl.completer = _comp
			if (r) repl.history.splice(0, 1)
			resolve(r)
		})
	})
}

/* eslint-disable @typescript-eslint/no-explicit-any */
async function askVarList(repl: any, varName: string, vlist: any) {
	const valuesList = []
	let haveValues = true
	while (haveValues) {
		const values = {}
		for (const v of vlist) await getVars(repl, values, v)
		haveValues = !!Object.keys(values).length
		if (haveValues) valuesList.push(values)
	}
	return valuesList
}

async function getVars(repl: any, values: View, v: any) {
	const varName: string = util.isObject(v) ? Object.keys(v)[0] : v
	if (values[varName]) return
	if (varName.endsWith('List')) {
		values[varName] = await askVarList(repl, varName, v[varName])
	} else if (varName.startsWith('has')) {
		if (
			'yes' ===
			(await question(
				repl,
				'yesno',
				chalk`Has this {green ${varName.replace('has', '')}}: `
			))
		)
			values[varName] = await askVarObjects(repl, varName, v[varName])
	} else if (varName.startsWith('not')) {
		const value = values[varName.replace('not', '')]
		if (value === undefined)
			for (const e of v[varName]) await getVars(repl, values, e)
	} else {
		const opt = util.isObject(v) ? 'optional' : 'required'
		const resp = await question(
			repl,
			varName,
			chalk`  {green ?} Value for {green ${varName}} (${opt}): `
		)
		if (resp) values[varName] = resp
	}
}

function allLocalSchemas(schemas: string[], view: View) {
	const local = /#\/components\/schemas\//
	if (view)
		for (const [k, v] of Object.entries(view)) {
			if ('object' === typeof v) allLocalSchemas(schemas, v)
			else if (k === '$ref' && !schemas.includes(v.replace(local, '')))
				schemas.push(v.replace(local, ''))
		}
	return schemas
}

export async function askVars(repl: any, name: string, tpl: string) {
	const values = {}
	console.log(chalk`Please enter values for the {green ${name}} template:\n`)
	for (const v of getParsedTemplate(tpl)) await getVars(repl, values, v)
	return values
}

export function apiGet(obj: View, path: string[]) {
	return path.length
		? obj[path[0]] && module.exports.apiGet(obj[path[0]], path.slice(1))
		: obj
}

export function apiSet(obj: View, path: string[], value: any): boolean {
	if (path.length > 1) {
		if (obj[path[0]]) return apiSet(obj[path.shift()], path, value)
	} else {
		obj[path[0]] = value
		return true
	}
	return false
}

export function apiValue(ctx: Context, args: string[] | string) {
	const cmdline = util.isArray(args) ? args : args.split(' ')
	const path =
		cmdline[0] === ''
			? ctx.apiPath
			: cmdline[0] === 'api'
			? cmdline.slice(1)
			: ctx.apiPath.concat(cmdline)
	let value = ctx.api
	for (const p of path) {
		if (typeof value[p] === 'undefined') return [undefined]
		value = value[p]
	}
	return [
		value,
		path.reduce((i: string, e: string) => i + "['" + e + "']", ''),
		path
	]
}

/* eslint-disable @typescript-eslint/no-explicit-any */
export async function fillTemplate(
	repl: any,
	tplDir: string,
	tplName: string,
	wsch?: OA3Schema
) {
	if (wsch) workSchema = wsch
	// read template
	const tpl = template(repl.context.argv, tplDir, tplName)
	if (!tpl) {
		console.error(
			chalk`Template {yellow ${tplDir}/${tplName}} not found or unreadable`
		)
		return null
	}
	// ask for vars of the template
	const values = await askVars(repl, tplName, tpl)
	// make mustache substitution
	const yamlSubst = mustache.render(tpl, values)
	// ask for confirmation showing prettyfied yaml substitution
	printYaml(yamlSubst)
	const confirmed = await question(repl, 'yesno', 'Is the above data OK? ')
	if (!confirmed || confirmed === 'no') {
		console.log(chalk`{yellow Aborted} - No API modification performed`)
		return null
	}
	// return view from yaml
	return fromYaml(yamlSubst)
}

export function addView(obj: View, view: View) {
	const key = Object.keys(view)[0]
	return key ? { ...obj, [key]: { ...obj[key], ...view[key] } } : obj
}

export function validOperationId(paths: View, operationId: string) {
	const op = Object.values(paths).find(p =>
		Object.values(p).find((o: OA3Operation) => o.operationId === operationId)
	)
	if (op)
		console.error(
			chalk`{yellow ERROR} - operationId {yellow ${operationId}} already exists`
		)
	return typeof op === 'undefined'
}
export function validSchemas(api: View, view: View) {
	const schemas = allLocalSchemas([], view)
	const missing = schemas.filter(
		(s: string) =>
			!Object.prototype.hasOwnProperty.call(api.components.schemas, s)
	)
	if (missing.length) {
		console.error(
			chalk`{yellow ERROR} - Found references to missing or non-local schemas:`,
			missing
		)
		console.error('Generation will CRASH if the problem persist !!!')
	}
	return missing.length === 0
}
