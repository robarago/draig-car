import chalk from 'chalk'
import util from 'util'
import { Context } from 'node:vm'

import { apiValue, question, apiSet, apiGet } from './common'
import {
	exitOrContinue,
	saveYaml,
	getLastQuery,
	getLastTransform
} from '../main/util'

const draigAnnotations: Record<string, (ctx?: Context) => string[]> = {
	delete: () => ['true', 'false'],
	get: () => ['true', 'false'],
	model: ctx => Object.keys(ctx.api.components.schemas),
	optional: ctx => Object.keys(ctx.api.components.schemas),
	patch: () => ['true', 'false'],
	post: () => ['true', 'false'],
	query: () => [getLastQuery() || 'knex('],
	'sch-recip': () => [],
	'sch-relation': ctx => ctx.model.allRels,
	'sch-type-autokey': () => ['true', 'false'],
	'sch-type-default': () => ['knex.fn.now()'],
	tableName: ctx => ctx.model.allTabs,
	transform: () => [getLastTransform() || '_.']
}

export async function annotateAction(args: string[] | string) {
	const ctx = this.context
	const [value, , path] = apiValue(ctx, args)
	if (typeof value === 'undefined' || !util.isObject(value)) {
		console.error(
			chalk`Sorry, can't create annotation under invalid path entry {green ${args}}`
		)
		return exitOrContinue(this)
	}
	const key = await question(
		this,
		Object.keys(draigAnnotations),
		chalk`Annotation key for {green ${path.join(' ')}} `
	)
	if (key === '') return exitOrContinue(this)
	if (!(key in draigAnnotations)) {
		console.error(
			chalk`Invalid annotation key {green ${key}}. ` +
				chalk`Valid ones are {green ${Object.keys(draigAnnotations).join(
					', '
				)}}`
		)
		return exitOrContinue(this)
	}
	try {
		const oldvalue = apiGet(ctx.api, [...path, `x-draig-${key}`]) || ''
		let newval: string | number = await question(
			this,
			[...draigAnnotations[key](ctx)],
			chalk`Annotation for {green ${key}} (C-P for oldvalue) `,
			oldvalue.replace(/\n/, '')
		)
		if (newval === '') return exitOrContinue(this)
		newval = isNaN(Number(newval)) ? newval : Number(newval)
		console.log(
			chalk`Annotation value: {green ${newval}} (type {green ${typeof newval}})`
		)
		// Horrible js-yaml hack to force | block in openapi-yaml generation
		if (typeof newval === 'string' && ['query', 'transform'].includes(key))
			newval = newval.trim() + '\n'
		apiSet(ctx.api, [...path, `x-draig-${key}`], newval)
		saveYaml(ctx.argv.api, ctx.api)
	} catch (e) {
		console.error(
			chalk`Annotation failed: {green ${e}} - Check for valid annotations`
		)
	}
	exitOrContinue(this)
}
