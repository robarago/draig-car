import util from 'util'
import chalk from 'chalk'

import { exitOrContinue, saveYaml } from '../main/util'
import { fillTemplate, apiValue } from './common'

export async function addArrayElemAction(args: string[]) {
	const ctx = this.context
	const argv = ctx.argv
	if (!args || !args.length) {
		console.log('Usage: ' + this.commands.addelem.help)
		return exitOrContinue(this)
	}
	// get value and path of object to be modified
	const [value, hashKey, path] = apiValue(ctx, args)
	if (!util.isArray(value)) {
		console.error(chalk`Object at {yellow ${args}} not found or not an array`)
		return exitOrContinue(this)
	}
	let schemaPath, workSchema
	if (path.length > 1) {
		schemaPath = [].concat(path).slice(0, -1)
		const toEval = schemaPath.reduce((i, v) => i + '["' + v + '"]', 'ctx.api')
		workSchema = eval(toEval)
	}
	const view = await fillTemplate(this, 'parts', path.pop(), workSchema)
	if (null === view) return exitOrContinue(this)
	// use substitution to modify current api definition
	eval('view.forEach(e => ctx.api' + hashKey + '.push(e))')
	saveYaml(argv.api, ctx.api)
	exitOrContinue(this)
}
