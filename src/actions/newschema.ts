import util from 'util'
import chalk from 'chalk'

import { exitOrContinue, saveYaml } from '../main/util'
import { fillTemplate } from './common'

export async function newSchemaAction(args: string[] | string) {
	const api = this.context.api
	const argv = this.context.argv
	if (!args || args.length < 1) {
		console.log('Usage: ' + this.commands.newsch.help)
		return exitOrContinue(this)
	}
	const name = util.isArray(args) ? args[0] : args.split(' ')[0]
	// read template
	const view = await fillTemplate(this, 'schema', name)
	if (null === view) return exitOrContinue(this)
	// check if schema already exists
	const schema = Object.keys(view)[0]
	if (schema in api.components.schemas) {
		console.error(chalk`Conflict - schema {yellow ${schema}} already exists`)
	} else {
		// add subtitution to new schema
		api.components.schemas[schema] = view[schema]
		console.error(chalk`Success - schema {green ${schema}} added`)
		saveYaml(argv.api, api)
	}
	exitOrContinue(this)
}
