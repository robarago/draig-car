import chalk from 'chalk'

import { exitOrContinue, saveYaml } from '../main/util'
import { apiValue, question, apiSet } from './common'

export async function editAction(args: string[] | string) {
	const ctx = this.context
	if (!args || !args.length) {
		console.log('Usage: ' + this.commands.edit.help)
		return exitOrContinue(this)
	}
	const [value, , path] = apiValue(ctx, args)
	if (typeof value === 'undefined' || typeof value === 'object') {
		console.error(
			chalk`Sorry, can't edit non-existent or object entry at {green ${args}}`
		)
		return exitOrContinue(this)
	}
	let newval: string | number = await question(
		this,
		[],
		chalk`Edit value for {green ${args}} `,
		value
	)
	newval = isNaN(Number(newval)) ? newval : Number(newval)
	console.log(
		chalk`New value: {green ${newval}} (type {green ${typeof newval}})`
	)
	if (typeof newval === 'string') newval = newval.trim()
	if (apiSet(ctx.api, path, newval)) saveYaml(ctx.argv.api, ctx.api)
	else
		console.error(
			chalk`Edit failed to set {green ${newval}} for {green ${path.join(' ')}})`
		)
	exitOrContinue(this)
}
