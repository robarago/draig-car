import yaml from 'js-yaml'
import chalk from 'chalk'

import { printYaml, exitOrContinue } from '../main/util'
import { apiValue } from './common'

export function printAction(args: string[] | string) {
	const ctx = this.context
	const [value] = apiValue(ctx, args)
	if (typeof value === 'undefined')
		console.log(chalk`Sorry, element at {yellow ${args}} not found`)
	else printYaml(yaml.safeDump(value))
	exitOrContinue(this)
}
