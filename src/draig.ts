#!/usr/bin/env node
import 'source-map-support/register'

import {
	initProject,
	generateAPI,
	testRequisites,
	startAPI,
	cleanGeneration
} from './main/util'
import { argv } from './main/args'
import { startRepl } from './main/repl'

async function main() {
	// Init command
	if (argv._.includes('init')) {
		initProject(argv)
		process.exit(0)
	}

	// Generate command
	if (argv._.includes('generate')) {
		testRequisites(argv)
		await generateAPI({ argv })
		process.exit(0)
	}

	// Serve command
	if (argv._.includes('serve')) {
		testRequisites(argv)
		startAPI({ argv }, true)
		process.exit(0)
	}

	// Clean command
	if (argv._.includes('clean')) {
		cleanGeneration({ argv })
		process.exit(0)
	}

	// Fallback to REPL
	await startRepl(argv)
}
main()
