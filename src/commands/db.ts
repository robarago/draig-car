import fs from 'fs'
import path from 'path'
import chalk from 'chalk'
//import { Knex } from 'knex'
import clearModule from 'clear-module'

import { exitOrContinue, loadConfig } from '../main/util'
import { generateSeed } from './genseed'
import { initCtx } from '../main/init'
import {
	contAction,
	pullImage,
	execInContainer,
	runContainer
} from '../main/container'

type DBResult = Record<string, () => void>
interface ResultCount {
	[rtype: string]: number
}
/*
type DBSupportFuncName = 'up' | 'down' | 'seed'
type DBSupportModule = {
	[id in DBSupportFuncName]: (knex: Knex.QueryBuilder) => DBResult[]
}
*/

function resultCount(r: DBResult[]): ResultCount {
	return r.reduce
		? r.reduce(
				(i: ResultCount, e: DBResult) =>
					!e
						? { ...i }
						: e.constructor
						? e.constructor.name in i
							? { ...i, [e.constructor.name]: i[e.constructor.name] + 1 }
							: { ...i, [e.constructor.name]: 1 }
						: { ...i },
				{}
		  )
		: { [r.constructor.name]: 1 }
}

export const desc = {
	help: 'Describe (show column info) a table in the Database',
	async action(name: string | string[]) {
		if (!name) {
			console.error('syntax: .desc <tableName>')
			return exitOrContinue(this)
		}
		const p = (t: number, s: string) =>
			(s + Array(t + 1).join(' ')).substring(0, t)
		const s = [30, 15, 9, 9, 40]
		const h = ['column', 'type', 'size', 'nullable', 'default']
		const info = await this.context.knex(name).columnInfo()
		if (Object.keys(info).length) {
			console.log(s.map((e, i) => p(e, h[i])).join(' '))
			console.log(s.map(e => Array(e + 1).join('-')).join(' '))
			for (const c in info) {
				const li = [
					c,
					info[c].type,
					info[c].maxLength == null ? '' : info[c].maxLength,
					info[c].nullable,
					info[c].defaultValue == null ? '' : info[c].defaultValue
				]
				console.log(s.map((e, i) => p(e, li[i])).join(' '))
			}
		} else console.log(chalk`Table or view {green ${name}} does not exist`)
		exitOrContinue(this)
	}
}

export const debug = {
	help: 'Toggle knex global debug mode for this session',
	action() {
		if (this.context.knex) {
			const c = this.context.knex.connection().client.config
			c.debug = !c.debug
			console.log(chalk`Debug is now {yellow ${c.debug}}`)
		} else
			console.log(
				chalk`Knex connection not available - Is the project generated?`
			)
		this.displayPrompt()
	}
}

export const migrate = {
	help: 'Migrate up or down the database using a file from migrations/ dir',
	async action(name: string | string[]) {
		const args = Array.isArray(name) ? name : name.split(' ')
		if (args.length !== 2 || !['up', 'down'].includes(args[0])) {
			console.error('syntax: .migrate <up|down> <filename>')
			return exitOrContinue(this)
		}
		const project = this.context.argv.projectDir
		const fname = fs.existsSync(args[1])
			? path.resolve('.', args[1])
			: path.resolve('.', project, 'migrations', args[1])
		try {
			clearModule(fname)
			const mf = args[0] === 'up' ? 'up' : 'down'
			const module = await import(fname)
			const results = await module[mf](this.context.knex)
			console.log(
				chalk`Migration {green ${args[0]}} with {green ${args[1]}} succeded:`,
				resultCount(results)
			)
		} catch (e) {
			console.error(chalk`Migration file {yellow ${args[1]}} failed:`, e)
		}
		exitOrContinue(this)
	}
}

export const seed = {
	help: 'Seed the database using a file from seeds/ dir',
	async action(name: string | string[]) {
		const args = Array.isArray(name) ? name : name.split(' ')
		if (args.length !== 1 || args[0] === '') {
			console.error('syntax: .seed <filename>')
			return exitOrContinue(this)
		}
		const project = this.context.argv.projectDir
		const fname = fs.existsSync(args[0])
			? path.resolve('.', args[0])
			: path.resolve('.', project, 'seeds', args[0])
		try {
			clearModule(fname)
			const module = await import(fname)
			const results = await module.seed(this.context.knex)
			console.log(chalk`Seeding with {green ${args[0]}}:`, results)
		} catch (e) {
			console.error(
				chalk`Unable to feed the DB with file {yellow ${args[0]}}`,
				e
			)
		}
		exitOrContinue(this)
	}
}

export const dbstop = {
	help: 'Stop DB container for current configuration',
	async action() {
		await contAction(this.context.argv.dbclient, 'exited', 'Stopping')
		exitOrContinue(this)
	}
}

export const dbstart = {
	help: 'Start DB container for current configuration',
	async action() {
		await contAction(this.context.argv.dbclient, 'running', 'Starting')
		exitOrContinue(this)
	}
}

export const dbreset = {
	help: '(Re)create DB container and the project database/user/schema',
	async action(name: string | string[]) {
		const args = Array.isArray(name) ? name : name.split(' ')
		if (args.length > 1 && args[0] !== 'destroy') {
			console.error('syntax: .dbreset [destroy]')
			return exitOrContinue(this)
		}
		const ctx = this.context
		if (ctx.knex) ctx.knex.destroy()
		const client = ctx.argv.dbclient
		try {
			await pullImage(client)
			const mycont = await runContainer(client, args[0] === 'destroy')
			if (mycont) await execInContainer(mycont, client)
			if (!ctx.argv._.includes('run')) {
				await initCtx(ctx)
				console.log(
					chalk`{yellow WARN}: Now you will need to run ` +
						chalk`{green generate, migrate} and {green seed} to ` +
						'restore your schema/tables and data !'
				)
			}
		} catch (e) {
			console.error(e)
		}
		exitOrContinue(this)
	}
}

export const genseed = {
	help: 'Generate the seed-database.js file from current DB schema',
	async action() {
		const ctx = this.context
		const projPath = path.resolve(ctx.argv.projectDir)
		const cfg = loadConfig() // Reload config
		ctx.argv.seedGen = cfg.seedGen
		// TODO: Check container, connection and DB schema readiness before this
		await generateSeed(
			this.context.knex,
			projPath,
			ctx.argv.seedGen,
			ctx.api.components.schemas
		)
		exitOrContinue(this)
	}
}
