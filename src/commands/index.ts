import * as apiedit from './apiedit'
import * as db from './db'
import * as project from './project'

export default { ...apiedit, ...db, ...project }
