/**
 * TODO:
 * 1.- La columnas que admiten nulos se deben crear con datos y nulos
 * 2.- Columnas de relación en las tablas, asignación de identificadores
 * 3.- Lanzar excepción sino se encuentra una correlación de datos Faker
 * 4.- Traducción de los nombres de columnas en las distintas BBDD
 * 4.- MetadataLocale (traducción de nombres de columnas a ingles)
 * 5.- Añadir helper a Faker
 */
import fs from 'fs'
import util from 'util'
import path from 'path'
import faker from 'faker'
import chalk from 'chalk'
import { Knex } from 'knex'

import { OA3Schema, OA3Property, OA3Ref } from '../types/api'
import { SeedGen, SeedTabConf } from '../types/config'

const modulesFaker = Object.keys(faker)
const regexpId = /(.*)_id/i
const randint = (n: number) => Math.floor(Math.random() * n) + 1

function isView(schemas: Record<string, OA3Schema>, tableName: string) {
	const sname = Object.keys(schemas).find(
		s =>
			schemas[s]['x-draig-tableName'] === tableName &&
			schemas[s]['x-draig-sch-raw']
	)
	return sname !== undefined
}

function getEnumList(
	schemas: Record<string, OA3Schema>,
	tableName: string,
	col: string
) {
	let enumList = []

	const enumOfCol = (table: string, col: string) =>
		table &&
		schemas[table] &&
		'properties' in schemas[table] &&
		col in schemas[table].properties &&
		schemas[table].properties[col]
			? 'enum' in schemas[table].properties[col]
				? (schemas[table].properties[col] as OA3Property).enum
				: 'type' in schemas[table].properties[col] &&
				  (schemas[table].properties[col] as OA3Property).type === 'boolean'
				? [true, false]
				: []
			: []

	// Which schema matches this table?
	const sname = Object.keys(schemas).find(
		s => schemas[s]['x-draig-tableName'] === tableName
	)

	// Look in the class properties for the enum col
	enumList = enumOfCol(sname, col)
	if (enumList.length > 0) return enumList

	// If there is not derived classes, finished
	if (!('allOf' in schemas[sname])) return []

	// Look for enums in derived classes
	const regex = /#\/components\/schemas\/(.*)/
	const matched = schemas[sname].allOf.map((d: OA3Ref) =>
		d['$ref'].match(regex)
	)
	for (const d of matched) {
		enumList = enumOfCol(d[1], col)
		if (enumList.length > 0) break
	}

	return enumList
}

function validateData(
	tables: Record<string, SeedTabConf>,
	data: Record<string, Record<string, unknown>[]>
) {
	for (const [table, tValue] of Object.entries(tables)) {
		const error: string[] = []
		data[table].forEach((d: Record<string, unknown>) => {
			for (const [name, value] of Object.entries(d)) {
				if (
					tValue.info[name] &&
					tValue.info[name].nullable === false &&
					(value === null || value === 'undefined')
				) {
					const e = `The column "${name}" does not allow null`
					if (!error.includes(e)) error.push(e)
				}
				const maxL = tValue.info[name] ? tValue.info[name].maxLength : undefined
				if (maxL && value && value.toString().length > maxL) {
					const e = `The size of column "${name}" cannot have length > ${maxL}`
					if (!error.includes(e)) error.push(e)
				}
			}
		})
		if (error && error.length !== 0) {
			return error
		}
	}
}

/**
 * Returns an object with the data to insert in the relation nameRel
 * @param {string} nameRel - Relation name. Format: table1_table2_....
 * @param {number} nRows - number of rows to insert
 * @param {object} infoCols table column information
 */
async function genRelationData(
	nameRel: string,
	nRows: number,
	infoCols: Record<string, Knex.ColumnInfo>
) {
	const cross = (n: number[], m: number[]) =>
		[].concat(...n.map((x: number) => m.map((y: number) => [].concat(x, y))))
	const makearr = (n: number) => [...Array(n).keys()].map(e => e + 1)
	const cartesian = (n: number, m: number) => cross(makearr(n), makearr(m))
	const namesCol = Object.keys(infoCols)
	const nmIds = cartesian(nRows, nRows)
	const result = []

	for (let i = 0; i < nRows; i += 1) {
		const ids = nmIds.splice(randint(nmIds.length) - 1, 1).flat()
		result.push({ [namesCol[0]]: ids[0], [namesCol[1]]: ids[1] })
	}

	return result
}

/**
 * Generate assignment structure of attribute table with function to get
 * the faker data.
 * @param tableName table name
 * @param values seed table generation config
 * @param infoCols table column information
 */
async function genDataFaker(
	tableName: string,
	values: SeedTabConf,
	schemas: Record<string, OA3Schema>
) {
	const infoCols = values.info
	const fnDefault = (col: string, inf: Knex.ColumnInfo) => {
		const elist = getEnumList(schemas, tableName, col)
		if (elist && elist.length > 0) return () => faker.random.arrayElement(elist)
		// ENUM typed columns should not be found past this line
		switch (inf.type.toUpperCase()) {
			case 'DATETIME':
			case 'TIMESTAMP':
			case 'TIMESTAMP WITHOUT TIME ZONE':
			case 'TIMESTAMP WITH TIME ZONE':
			case 'TIMESTAMP(6) WITH LOCAL TIME ZONE':
				return () => {
					return faker.date.past().toISOString()
				}
			case 'BOOLEAN':
				return () => {
					return faker.datatype.boolean()
				}
			case 'NUMBER':
			case 'INTEGER':
			case 'INT':
			case 'BIGINT':
				return () => {
					return faker.datatype.number(
						inf.maxLength ? { min: 1, max: inf.maxLength } : undefined
					)
				}
			case 'TINYINT':
				return () => {
					return faker.datatype.number({ min: 0, max: 1 })
				}
			case 'DECIMAL':
				return () => {
					return faker.datatype.number(
						inf.maxLength
							? { min: 1, max: inf.maxLength, precision: 0.01 }
							: { precission: 0.01 }
					)
				}
			case 'TEXT':
			case 'LONGTEXT':
			case 'VARCHAR2':
			case 'VARCHAR':
			case 'CHARACTER VARYING':
				return () => {
					const str = faker.random.word()
					if (inf.maxLength) {
						return str.substring(0, inf.maxLength)
					}
					return str
				}
			default:
				console.log(
					`Unknown TYPE for ${tableName}.${col}: ${inf.type.toUpperCase()}`
				)
				if (inf.defaultValue) {
					return () => {
						return inf.defaultValue
					}
				} else {
					return undefined
				}
		}
	}
	const fnData = (col: string, inf: Knex.ColumnInfo) => {
		let fixedVals: string[] = []
		if ('cols' in values && col in values.cols) {
			if (typeof values.cols[col] === 'string') {
				return () => {
					const val = faker.fake(values.cols[col])
					switch (inf.type.toLowerCase()) {
						// Json type is for arrays
						case 'json':
						case 'longtext':
							return JSON.stringify(
								faker.datatype
									.array(faker.datatype.number({ min: 1, max: 3 }))
									.fill(val)
							)
						case 'int':
						case 'integer':
						case 'number':
							return Number(val)
						case 'timestamp':
						case 'timestamp(6) with local time zone':
							return new Date(Date.parse(val))
						default:
							//console.warn(`Warning: unknown type ${inf.type} for col ${col}`)
							return val
					}
				}
			} else {
				// Fixed bag (array) of values sequentially sampled
				fixedVals = [...values.cols[col]]
				return () => fixedVals.pop()
			}
		}
		if (regexpId.test(col)) {
			// 5 are the default number of rows generated
			return () => randint(5)
		}
		for (const g of modulesFaker) {
			const res = Object.keys(faker[g]).find(
				o =>
					o.toLowerCase().indexOf(col.toLowerCase()) != -1 &&
					typeof faker[g][o] === 'function'
			)
			if (res !== undefined) {
				const fn = faker[g][res]
				Object.defineProperty(fn, 'name', { value: res, configurable: true })
				//console.warn(chalk`Using faker ${fn.name} found for {green ${tableName}.${col}}`)
				return fn
			}
		}
		console.warn(
			chalk`Faker fn not found for {green ${tableName}.${col}} - using defaults`
		)
		return fnDefault(col, inf)
	}
	return Object.keys(infoCols).reduce(
		(i, e) => (e !== 'id' ? { ...i, [e]: fnData(e, infoCols[e]) } : i),
		{}
	)
}

/**
 * Generate seed data file using faker helpers and seed configuration.
 *
 * @param {knex}     Knex for getting columns info
 * @param {projPath} Initial path to the project
 * @param {seedgen}  seeding configuration (from draig.yaml file)
 * @param {schemas}  Schemas from the api.yaml file
 */
export async function generateSeed(
	knex: Knex,
	projPath: string,
	seedgen: SeedGen,
	schemas: Record<string, OA3Schema>
) {
	const { allTabs, allRels } = await import(path.join(projPath, 'models'))
	console.log(
		chalk`Generating seed-database.js for tables {green ${allTabs.join(', ')}}${
			allRels.length ? ' and relations ' : ''
		}{green ${allRels.join(', ')}}`
	)

	// Build config from seedGen arrays - it will be modified
	const config = {
		localeData: 'es',
		tabs: {
			...allTabs.reduce(
				(i: SeedTabConf, n: string) => ({ ...i, [n]: { rows: 5 } }),
				{}
			),
			...allRels.reduce(
				(i: SeedTabConf, n: string) => ({
					...i,
					[n]: { relation: true, rows: 5 }
				}),
				{}
			),
			...seedgen.tabs
		}
	}

	// Remove views from tabs list
	const allTables: Record<string, SeedTabConf> = Object.keys(
		config.tabs
	).reduce(
		(i, n) => (!isView(schemas, n) ? { ...i, [n]: config.tabs[n] } : i),
		{}
	)
	// Configure locale
	faker.locale = config['localeData'] || faker.locale
	// Add info data to columns
	for (const [key, value] of Object.entries(allTables)) {
		const info = await knex(key)
			.columnInfo()
			.catch(err => {
				console.error(
					chalk`{red Error}: Can't run columnInfo() in DB: ${err} - ` +
						chalk`Is the container running ?` +
						chalk`{yellow Tip}: run {green dbreset [destroy]} and then ` +
						chalk`{green migrate up} to restore your schema`
				)
				return {}
			})
		if (!Object.keys(info).length) {
			console.error(
				chalk`Table or view does not exists: {green ${key}} ` +
					chalk`- Did you forget to run {green migrate up} ?`
			)
			return false
		}
		allTables[key] = { ...value, info }
	}
	const tables = Object.keys(allTables).filter(t => !allTables[t].relation)
	const relations = Object.keys(allTables).filter(t => allTables[t].relation)

	// Set and reset (at the end of the func) generation conditions
	const initialTZ = process.env.TZ

	// For this generation only
	process.env.TZ = 'UTC'

	// Add faker fns for every column
	let fakerTables = {}
	for (const t of tables) {
		const values = allTables[t]
		fakerTables = {
			...fakerTables,
			[t]: await genDataFaker(t, values, schemas)
		}
	}

	// Generate data for tables
	let genData: Record<string, Record<string, unknown>[]> = {}
	// Generate data tables
	for (const [key, value] of Object.entries(fakerTables)) {
		const data = []
		const table: SeedTabConf = allTables[key]
		for (let i = 0; i < (table.rows || 5); i++) {
			let d = {}
			for (const [n, fn] of Object.entries(value)) {
				let res =
					typeof fn === 'function'
						? fn(
								'datetime' === fn.name
									? { min: 2724302168, max: 2150196367003 }
									: 'randint' === fn.name
									? table.rows
									: undefined
						  )
						: fn
				const tMaxLength = table.info[n].maxLength || undefined
				while (tMaxLength && res && res.length > tMaxLength)
					res = res.substring(0, tMaxLength)
				// Date must be coerced to string
				if (util.isDate(res)) res = res.toISOString()
				d = { ...d, [n]: res }
			}
			data.push(d)
		}
		if ('addrows' in table && Array.isArray(table.addrows)) {
			console.log(chalk`Adding extra rows to {green ${key}}:`, table.addrows)
			data.push(...table.addrows)
		}
		genData = { ...genData, [key]: data }
	}

	// Generate data relations
	for (const r of relations) {
		const e = allTables[r]
		genData = {
			...genData,
			[r]: await genRelationData(r, e.rows, e.info)
		}
	}

	// Validate data
	validateData(allTables, genData)

	// Generate file
	const data =
		'const db = ' +
		util.inspect(genData, { depth: 3 }) +
		'\nmodule.exports = db\n'
	fs.writeFileSync('seed-database.js', data, 'utf-8')

	// Reset initial configuration
	process.env.TZ = initialTZ

	return true
}
