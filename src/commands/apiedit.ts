import actions from '../actions'

export const addelem = {
	help: '.addelem <apiPathToArray>: add a new element to an existing API array',
	action(name: string | string[]) {
		actions.addArrayElemAction.bind(this)(name)
	}
}
export const addprop = {
	help: '.addprop <schema>: add a new property to an existing schema',
	action(name: string | string[]) {
		actions.addPropertyAction.bind(this)(name)
	}
}
export const annotate = {
	help: '.annotate <apiPath>: add a draig annotation under an existing API path',
	action(name: string | string[]) {
		actions.annotateAction.bind(this)(name)
	}
}
export const newsch = {
	help:
		'.newsch <template>: add a new schema to the component/schemas ' +
		'catalog of current API using schema/<template>',
	action(name: string | string[]) {
		actions.newSchemaAction.bind(this)(name)
	}
}
export const newop = {
	help:
		'.newop <template>: add a new operation to ' +
		'current API definition using op/<template>',
	action(name: string | string[]) {
		actions.newOperationAction.bind(this)(name)
	}
}
export const edit = {
	help: '.edit <apiPath>: edit existing (string or number) api PATH element',
	action(name: string | string[]) {
		actions.editAction.bind(this)(name)
	}
}
export const rm = {
	help:
		'.rm <apiPath>: remove existing object from ' +
		'current API definition - i.e.: .rm paths /example post',
	action(name: string | string[]) {
		actions.rmAction.bind(this)(name)
	}
}
export const ls = {
	help: '.ls <apiPath>: List object keys of the given API path',
	action(name: string | string[]) {
		actions.lsAction.bind(this)(name)
	}
}
export const cd = {
	help: 'Change API working object to the given argument',
	action(name: string | string[]) {
		actions.cdAction.bind(this)(name)
	}
}
export const print = {
	help: '.print <apiPath>: Recursivelly print properties under API path',
	action(name: string | string[]) {
		actions.printAction.bind(this)(name)
	}
}
