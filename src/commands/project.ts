import { Context } from 'node:vm'

import { initCtx } from '../main/init'
import {
	listCmds,
	runPackager,
	startAPI,
	stopAPI,
	statusAPI,
	generateAPI,
	cleanGeneration,
	loadConfig
} from '../main/util'

async function runInContext(
	ctx: Context,
	func: (c: Context) => void,
	clear: boolean
) {
	const failed = await func.call(null, ctx)
	if (ctx.argv._.includes('run')) process.exit(0)
	if (clear && !failed) await initCtx(ctx)
	ctx.repl.displayPrompt()
}

export const list = {
	help: 'Show list of available internal runnable commands',
	action() {
		runInContext(this.context, c => listCmds(c), false)
	}
}
export const test = {
	help: 'Run "yarn test" into the API server project directory',
	async action() {
		await runInContext(this.context, c => runPackager(c, 'test'), true)
	}
}
export const install = {
	help: 'Run "yarn install" into the API server project directory',
	action() {
		runInContext(this.context, c => runPackager(c, 'install'), true)
	}
}
export const start = {
	help: 'Start the API server',
	action() {
		runInContext(this.context, c => startAPI(c, false), false)
	}
}
export const stop = {
	help: 'Stop the API server',
	action() {
		runInContext(this.context, c => stopAPI(c), false)
	}
}
export const status = {
	help: 'Check the status of the API server',
	action() {
		runInContext(this.context, c => statusAPI(c), false)
	}
}
export const generate = {
	help: '(Re)generate the API project using current API definition',
	action() {
		runInContext(this.context, c => generateAPI(c), true)
	}
}
export const clean = {
	help: 'Clean all generated artifacts and log files',
	action() {
		runInContext(this.context, c => cleanGeneration(c), false)
	}
}
export const config = {
	help: 'Show current project configuration',
	action() {
		runInContext(this.context, () => console.log(loadConfig()), false)
	}
}
