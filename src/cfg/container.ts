interface ContainerCfg {
	[key: string]: {
		img: string
		port: number
		env: string[]
		t: number
		cmd: string[]
		lines: string[]
	}
}

export default ({
	dbUserName: user,
	dbName: db,
	dbUserPassword: pwd
}: {
	dbUserName: string
	dbName: string
	dbUserPassword: string
}): ContainerCfg => ({
	pg: {
		img: 'postgres:16-alpine',
		port: 5432,
		env: [`POSTGRES_PASSWORD=draig`],
		t: 10,
		cmd: ['psql', '-a', '-U', 'postgres'],
		lines: [
			`drop database if exists ${db};`,
			`revoke all on schema public from ${user};`,
			`drop role if exists ${user};`,
			`create database ${db};`,
			`create user ${user} encrypted password '${pwd}';`,
			`alter database ${db} owner to ${user};`
		]
	},
	mysql: {
		img: 'mariadb:11.4.2',
		port: 3306,
		env: ['MYSQL_ROOT_PASSWORD=draig'],
		t: 20,
		cmd: ['mariadb', '-v', '-pmysql'],
		lines: [
			`drop database if exists ${db};`,
			`drop user if exists '${user}'@'%';`,
			`create database ${db};`,
			`grant usage on *.* to '${user}'@'%' identified by '${pwd}';`,
			`grant all privileges on ${db}.* to '${user}'@'%';`,
			'quit'
		]
	},
	oracledb: {
		img: 'gvenzl/oracle-free:slim',
		port: 1521,
		env: ['ORACLE_PASSWORD=draig'],
		t: 60,
		cmd: ['sqlplus', '-S', 'sys/oracle', 'as', 'sysdba'],
		lines: [
			`drop user ${user} cascade;`,
			`create user ${user} identified by "${pwd}" quota unlimited on USERS;`,
			`grant connect, resource, create view to ${user};`,
			'quit'
		]
	}
})
