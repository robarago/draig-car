import fs from 'fs'
import path from 'path'
import figl from 'figlet'
import chalk from 'chalk'
import yaml from 'js-yaml'
import Tail from 'tail-file'
import mustache from 'mustache'
import { Context } from 'node:vm'
import colorize from 'json-colorizer'
import cmdexists from 'command-exists'
import { spawn, spawnSync } from 'child_process'
import { highlight, parse } from 'cli-highlight'

import { View, OA3Schema, OA3Object } from '../types/api'
import { generateSeed } from '../commands/genseed'
import { Arguments } from './args'

interface Err {
	code?: string
	message?: string
	files?: string[]
}

// OA3 code generator command
let codegen: string
// OA3 code generator switch for 'generate'
let gswitch: string
// JS packager to use: npm or yarn
let packager: string
// Last knex query issued in the REPL
let lastquery: string
// Last transformation issued in the REPL
let lasttransform: string
// Spinner status
let spin_seq = 0

// Temporary hacks to make depending TS modules happy
export function runnable(cmd: string) {
	return !['cd', 'debug'].includes(cmd)
}

export function setLastQuery(q: string) {
	lastquery = q
}

export function setLastTransform(t: string) {
	lasttransform = t
}

export function getLastQuery(): string {
	return lastquery
}

export function getLastTransform(): string {
	return lasttransform
}

/* eslint-disable @typescript-eslint/no-explicit-any */
function spinner(file: number, source: string, data: any) {
	const spinchar = ['|', '/', '-', '\\']
	const now = new Date().toISOString()
	spin_seq = spin_seq == 3 ? 0 : spin_seq + 1
	process.stdout.write('\x1B[?25l')
	process.stdout.write(spinchar[spin_seq] + '\b')
	fs.writeSync(file, `${now} ${source} ${data}`)
}

function spawnProc(cmdarr: string[], logfile: string, cwd?: string) {
	return new Promise((resolve, reject) => {
		const logFile = fs.openSync(logfile, 'a')
		const subprocess = spawn('sh', ['-c', cmdarr.join(' ')], { cwd })
		process.stdout.write(chalk`{green ${cmdarr.join(' ')}}... `)
		subprocess.on('error', err => reject(err))
		subprocess.on('close', data => {
			process.stdout.write('\x1B[?25h')
			process.stdout.write(
				chalk`\r{green ${cmdarr.join(' ')}} DONE (rc = {yellow ${data}})\n`
			)
			fs.closeSync(logFile)
			resolve(undefined)
		})
		subprocess.stderr.on('data', d => spinner(logFile, 'ERR', d))
		subprocess.stdout.on('data', d => spinner(logFile, 'OUT', d))
	})
}

export function loadConfig() {
	const config = path.resolve(process.cwd(), 'draig.yaml')
	if (!fs.existsSync(config)) {
		console.error('Project configuration file "draig.yaml" does not exists!')
		process.exit(1)
	}
	return yaml.safeLoad(fs.readFileSync(config))
}

function findEmpty(schemas: NodeJS.Dict<OA3Schema>) {
	const emptySchemas = Object.keys(schemas).filter(
		o =>
			(schemas[o].properties && !Object.keys(schemas[o].properties).length) ||
			(schemas[o].allOf &&
				schemas[o].allOf.some(
					(e: OA3Object) =>
						'properties' in e && !Object.keys(e.properties).length
				))
	)
	return emptySchemas
}

export function runPackager(ctx: Context, cmd: string) {
	const cmdline = packager === 'npm' && cmd !== 'install' ? 'run ' + cmd : cmd
	return spawnProc([packager, cmdline], 'packager.log', ctx.argv.projectDir)
}

function saveAPIpid(ctx: Context, pid: number) {
	fs.writeFileSync(path.resolve(ctx.argv.projectDir, 'api.pid'), pid.toString())
}

function readAPIpid(ctx: Context): number {
	const pidFile = path.resolve(ctx.argv.projectDir, 'api.pid')
	return fs.existsSync(pidFile) ? Number(fs.readFileSync(pidFile)) : null
}

function removeAPIpid(ctx: Context) {
	const pidFile = path.resolve(ctx.argv.projectDir, 'api.pid')
	if (fs.existsSync(pidFile)) fs.unlinkSync(pidFile)
}

export function saveYaml(filename: string, jsText: string) {
	fs.writeFileSync(filename, yaml.safeDump(jsText, { flowLevel: -1 }))
}

export function fromYaml(yamlText: string) {
	return yaml.safeLoad(yamlText)
}

export function printYaml(yamlText: string) {
	console.log(
		highlight(yamlText, {
			language: 'yaml',
			theme: parse(
				'{ "literal": ["whiteBright", "bold"], "string": "whiteBright", "comment": "greenBright", "attr": "cyanBright", "number": ["whiteBright", "bold"] }'
			)
		})
	)
}

export function initProject(vars: Arguments) {
	function logVar(name: string, variable: string) {
		if (variable) console.log(chalk`  ${name}: {green ${variable}}`)
	}
	function applyTemplate(tmpl: string, dst: string, view: View) {
		const data = fs.readFileSync(
			path.resolve(__dirname, '..', 'tpl', 'init', tmpl),
			{ encoding: 'utf8' }
		)
		const rendered = mustache.render(data, view)
		fs.writeFileSync(dst, rendered, 'utf8')
		console.log(chalk`Configuration file {green ${dst}} written successfully`)
	}

	console.log(chalk`\nInitializing project: {green ${vars.project}}\n`)
	logVar('DB Client     ', vars.dbClient)
	logVar('DB name       ', vars.dbName)
	logVar('DB username   ', vars.dbUserName)
	logVar('DB password   ', vars.dbUserPassword)
	logVar('DB host       ', vars.dbHost)
	console.log('')
	applyTemplate('draig.mustache', 'draig.yaml', vars)
	if (fs.existsSync(`${vars.project}.yaml`))
		console.log(
			chalk`Using existing API definition {green ${vars.project}.yaml}`
		)
	else applyTemplate('api.mustache', `${vars.project}.yaml`, vars)
	console.info('\nNext steps:\n')
	let i = 1
	console.info(
		chalk`{cyan ${(i += 1)}.} Check that configuration files are correct`
	)
	if (vars.dbClient !== 'sqlite3')
		console.info(
			chalk`{cyan ${(i += 1)}.} Execute \`{green draig run dbreset [destroy]}'`
		)
	if (!vars.seedGen || !vars.seedGen.useFaker)
		console.info(
			chalk`{cyan ${(i += 1)}.} Create or copy here your '{green seed-database.js}' file`
		)
	console.info(chalk`{cyan ${(i += 1)}.} Generate project`)
	console.info(chalk`{cyan ${(i += 1)}.} Enjoy !\n`)
}

export function setPrompt(ctx: Context) {
	if (ctx.apiPath) {
		ctx.repl.setPrompt(
			chalk`{cyan ${ctx.api.info.title}:} ${ctx.apiPath.join(' ')}{bold >} `
		)
	} else ctx.repl.setPrompt(chalk`{cyan no api}:{bold >} `)
}

export async function showBanner() {
	const p = await import(path.resolve(__dirname, '..', '..', 'package.json'))
	const v = p.version.split('.')
	console.log(
		chalk`{green ${figl.textSync(p.name)}} ` +
			chalk`v{redBright ${v[0]}}.{greenBright ${v[1]}}.{blueBright ${v[2]}}\n`
	)
	const t = p.description.split('\n')
	const title = t[0]
		.split(' ')
		.map((w: string) => `${chalk.green(w.substring(0, 1))}${w.substring(1)}`)
		.join(' ')
	console.log(title)
	console.log(chalk.green(t[1] + '\n'))
	console.log(
		`Copyright (C) 2019-2022 Roberto Aragón\n` +
			`This program comes with ABSOLUTELY NO WARRANTY; for details\n` +
			chalk`run {green .license} in the REPL. This is free software, and\n` +
			`you are welcome to redistribute it under certain conditions.\n` +
			chalk`Run {green .license} in the REPL for details.\n`
	)
}

export function testRequisites(argv: Arguments) {
	const javaExists = cmdexists.sync('java')
	const oagExists = cmdexists.sync('openapi-generator')
	const sgcExists = cmdexists.sync('swagger-codegen')
	const yarnExists = cmdexists.sync('yarn')
	if (!javaExists || (!oagExists && !sgcExists)) {
		console.error(
			chalk`\nYou need {green java} (a JDK) and {green openapi-generator}` +
				chalk` or {green swagger-codegen} in your PATH\n`
		)
		process.exit(1)
	}
	if (sgcExists) {
		codegen = 'swagger-codegen'
		gswitch = '-l'
	} else {
		codegen = 'openapi-generator'
		gswitch = '-g'
	}
	packager = yarnExists ? 'yarn' : 'npm'
	console.log(chalk`> Packager: {green ${packager}}`)
	if (!argv.silent) console.log(chalk`> Codegen provider: {green ${codegen}}`)
}

export function stopAPI(ctx: Context) {
	const pid = readAPIpid(ctx)
	if (pid) {
		console.log('Killing process', pid)
		if (process.platform === 'win32') {
			spawn('taskkill', ['/pid', String(pid), '/f', '/t'], {
				stdio: 'ignore'
			})
		} else process.kill(pid, 'SIGTERM')
		removeAPIpid(ctx)
		if (ctx.tail) {
			ctx.tail.stop()
			delete ctx.tail
		}
	} else console.log('No current API process currently running')
}

export function statusAPI(ctx: Context) {
	const pid = readAPIpid(ctx)
	if (pid) console.log('The API server is running with pid', pid)
	else console.log('API server is NOT running')
}

export function startAPI(ctx: Context, sync: boolean) {
	const pid = readAPIpid(ctx)
	let checks = 50 // 50 * 100ms = 5s
	const replsrv = ctx.repl
	if (pid) return console.log('API already running with pid: %d', pid)
	const spwn = sync ? spawnSync : spawn
	const checkStartTail = (logfile: string) => {
		if (checks === 0) {
			// Give up
			console.warn(chalk`\n{red APIServer}: unable to start tail after 5s`)
			replsrv.displayPrompt(true)
			return
		}
		if (!fs.existsSync(logfile)) {
			setTimeout(checkStartTail, 100, logfile)
			checks -= 1
		} else {
			ctx.tail = new Tail(logfile)
			ctx.tail.on('line', (data: any) => {
				console.log(
					chalk`\n{green APIServer}: ` +
						colorize(data, {
							pretty: true,
							colors: {
								BRACE: 'green.bold',
								COMMA: 'green.bold',
								COLON: 'green.bold',
								STRING_KEY: 'blue',
								STRING_LITERAL: 'white',
								NUMBER_LITERAL: 'yellow',
								NULL_LITERAL: 'red'
							}
						})
				)
				replsrv.displayPrompt(true)
			})
			ctx.tail.on('error', (err: Err) => {
				console.log(chalk`\n{brown ERROR} - Tail failure: `, err)
				replsrv.displayPrompt(true)
			})
			ctx.tail.start()
		}
	}
	try {
		const subprocess = spwn('sh', ['-c', packager + ' start'], {
			detached: !sync,
			stdio: sync ? 'inherit' : 'ignore',
			cwd: ctx.argv.projectDir
		})
		if (!sync) {
			console.log('API started with pid', subprocess.pid)
			//subprocess.unref()
			saveAPIpid(ctx, subprocess.pid)
			const logfile = path.resolve(ctx.argv.projectDir, 'combined.log')
			checkStartTail(logfile)
		}
	} catch (e) {
		console.error('Error starting API server: ' + e)
		console.error('Generate and install the API server before starting it')
	}
}

export async function generateAPI(ctx: Context) {
	if (!ctx.api) ctx.api = yaml.safeLoad(fs.readFileSync(ctx.argv.api))
	const emptySchemas = findEmpty(ctx.api.components.schemas)
	if (emptySchemas.length) {
		console.error(
			chalk`{yellow ERROR}: Can't generate project - found empty local schemas:`,
			emptySchemas
		)
		return true // failure - no need to reset CTX
	}
	// Check before generation !
	const isDBClientChanged = fs.existsSync(ctx.argv.projectDir)
		? ctx.argv.dbclient !==
		  (await import(path.resolve(path.join(ctx.argv.projectDir, 'knexfile'))))
				.development.client
		: false
	// Global installation of draig-oag or @openapitools/openapi-generator required
	let args = [
		'generate',
		'-i',
		ctx.argv.api,
		gswitch,
		ctx.argv.generator,
		'-o',
		ctx.argv.projectDir,
		'-c',
		'draig.yaml'
	]
	if (ctx.argv.template) args = args.concat(['-t', ctx.argv.template])
	console.log('Generating API... ')
	if (await spawnProc([codegen, ...args], 'generator.log')) {
		console.error(
			chalk`Generation failed - Please, check {green generator.log} file`
		)
		return true
	}
	if (ctx.argv.noinstall) {
		console.log('Not preparing project')
		return true
	}

	const fname = '01_create_tables.js'
	const nfname = fname + '.new'
	const migrations = fs.existsSync('migrations')
		? path.resolve('.', 'migrations')
		: path.resolve('.', ctx.argv.projectDir, 'migrations')
	// If old sch exists the schema is new
	const isNew = !fs.existsSync(path.resolve(migrations, fname))
	let isChanged = false
	if (!isNew) {
		const newsch = fs.readFileSync(path.resolve(migrations, nfname))
		const oldsch = fs.readFileSync(path.resolve(migrations, fname))
		// If old sch != new sch, then the schema has changed
		isChanged = 0 !== Buffer.compare(newsch, oldsch) || isDBClientChanged
	}
	console.log('Preparing project - please wait...')
	await runPackager(ctx, 'install')
	console.log(
		chalk`Schema / DB client is {green ${
			isNew ? 'new' : isChanged ? 'changed' : 'unchanged'
		}}`
	)
	if (isChanged && !isDBClientChanged) await runPackager(ctx, 'migrate-down')
	if (fs.existsSync(path.resolve(migrations, nfname)))
		fs.renameSync(
			path.resolve(migrations, nfname),
			path.resolve(migrations, fname)
		)
	if (isNew || isChanged) await runPackager(ctx, 'migrate-up')
	const projPath = path.resolve(ctx.argv.projectDir)
	if (!fs.existsSync(path.join(projPath, 'utils', 'orm.js'))) return true
	const orm = await import(path.join(projPath, 'utils', 'orm.js'))
	const knex = orm.default.knex()
	const cfg = loadConfig()
	ctx.argv.seedGen = cfg.seedGen
	if (!ctx.argv.seedGen.useFaker) return true

	if (
		await generateSeed(
			knex,
			projPath,
			ctx.argv.seedGen,
			ctx.api.components.schemas
		)
	) {
		console.log(chalk`{green genseed} DONE`)
		await runPackager(ctx, 'seed')
	}
}

export function templateList(argv: Arguments, dir: string) {
	const list: string[] = []
	const userPath = path.resolve(
		path.dirname(path.resolve(argv.api)),
		'tpl',
		dir
	)
	const sysPath = path.resolve(__dirname, '..', '..', 'tpl', dir)
	if (fs.existsSync(userPath)) list.push(...fs.readdirSync(userPath))
	list.push(...fs.readdirSync(sysPath))
	return list.map(e => e.split('.')[0])
}

export function template(argv: Arguments, dir: string, name: string) {
	// look first user templates (=apidir + tpl + dir as api definition file)
	const userTpl = path.resolve(path.dirname(path.resolve(argv.api)), 'tpl', dir)
	let f = path.resolve(userTpl, name + '.mustache')
	if (fs.existsSync(f)) return fs.readFileSync(f, 'utf8')
	// then look in the draig templates class dir for template
	f = path.resolve(__dirname, '..', '..', 'tpl', dir, name + '.mustache')
	if (fs.existsSync(f)) return fs.readFileSync(f, 'utf8')
	return null
}

export function configProject(argv: Arguments) {
	// Set api and projectDir based on current configuration file
	const cfg = loadConfig()
	if (!argv.projectDir && !cfg.apiProject) {
		console.error(
			'You must specify a `project-dir\' in args or an `apiProject\' in "draig.yaml"!'
		)
		process.exit(1)
	}
	argv.api = argv.projectDir ? argv.projectDir + '.yaml' : cfg.apiProject
	argv.projectDir =
		argv.projectDir || cfg.apiProject.replace(path.extname(argv.api), '')
	argv.generator = cfg.openapiGenerator || 'draig'
	argv.dbclient = cfg.dbClient
	argv.seedGen = cfg.seedGen
}

export function listCmds(ctx: Context) {
	const replsrv = ctx.repl
	const commands = replsrv.commands
	for (const c of Object.keys(commands))
		if (runnable(c)) console.log(chalk`{green %s}: %s`, c, commands[c].help)
}

export function exitOrContinue(repl: any) {
	if (repl.context.argv._.includes('run')) process.exit(0)
	else repl.displayPrompt(true)
}

export function cleanGeneration(ctx: Context) {
	if (fs.existsSync(ctx.argv.projectDir))
		fs.rmdirSync(ctx.argv.projectDir, { recursive: true })
	if (fs.existsSync('generator.log')) fs.unlinkSync('generator.log')
	if (fs.existsSync('packager.log')) fs.unlinkSync('packager.log')
	if (fs.existsSync('seed-database.js')) fs.unlinkSync('seed-database.js')
}
