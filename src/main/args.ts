import yargs from 'yargs'

import { runnable, configProject } from './util'
import { SeedGen } from '../types/config'
import commands from '../commands'

export interface Arguments {
	[x: string]: unknown
	project: string
	port: number
	dbClient: 'mysql' | 'sqlite3' | 'pg' | 'oracledb'
	dbName: string
	dbUserName: string
	dbUserPassword: string
	dbHost: string
	generator: string
	'project-dir': string
	noinstall: boolean
	template: string
	intcmd: string
	intargs: string
	api?: string
	seedGen?: SeedGen
	_: (string | number)[]
}

export const argv = yargs
	.usage('Usage: $0 <init|repl|generate|serve|run>')
	.command(
		'init <project> <port> <dbClient> <dbName>',
		'Create draig artifacts and YAML to initialize generation project',
		yargs =>
			yargs
				.positional('project', {
					description: 'API Project file name',
					type: 'string'
				})
				.positional('port', {
					description: 'Port where the API will be reachable',
					type: 'number'
				})
				.positional('dbClient', {
					description: 'Backend connector for the object relational mapper',
					choices: ['mysql', 'sqlite3', 'pg', 'oracledb'] as const,
					demandOption: true
				})
				.positional('dbName', {
					description:
						"Name for the database (or file) name (use `XE' for oracledb)",
					type: 'string'
				})
				.option('generator', {
					alias: 'g',
					default: 'draig',
					description: 'OpenAPI3 generator name (default: draig)',
					type: 'string'
				})
				.option('dbUserName', {
					alias: 'u',
					description: 'DB connection user',
					type: 'string'
				})
				.option('dbUserPassword', {
					alias: 'p',
					description: 'DB connection password',
					type: 'string'
				})
				.option('dbHost', {
					alias: 'h',
					default: 'localhost',
					description: 'DB connection host',
					type: 'string'
				}),
		args => {
			if (
				args.dbClient !== 'sqlite3' &&
				(!args.dbUserName || !args.dbUserPassword)
			) {
				yargs.showHelp()
				console.log(
					`\nSorry, ${args.dbClient} requires user and password to be specified`
				)
				process.exit(1)
			}
			if (isNaN(args.port) || args.port < 1025 || args.port > 65535) {
				yargs.showHelp()
				console.log('\nSorry, the port number is invalid (range 1024-65535)')
				process.exit(1)
			}
		}
	)
	.command(
		'repl [project-dir]',
		'Open draig repl to begin generation or maintenance mode',
		yargs =>
			yargs.positional('project-dir', {
				describe: 'Directory name where the api projects are generated',
				type: 'string'
			})
	)
	.command(
		'generate [project-dir]',
		'Generate project with current configuration',
		yargs =>
			yargs
				.positional('project-dir', {
					describe: 'Directory name where the api projects are generated',
					type: 'string'
				})
				.option('noinstall', {
					alias: 'i',
					describe: "Don't execute install after generating",
					type: 'boolean'
				})
				.option('template', {
					alias: 't',
					describe:
						'Draig generator template folder to override default templates',
					type: 'string'
				})
	)
	.command(
		'clean',
		'Clean (remove) all generated artifacts (including log files)'
	)
	.command(
		'serve [project-dir]',
		'Serve API from specified project-dir (if it is generated + installed)',
		yargs =>
			yargs.positional('project-dir', {
				describe: 'Directory name where the api projects are generated',
				type: 'string'
			})
	)
	.command(
		'run <intcmd|list> [intargs..]',
		'Open draig repl and run specified internal command with optional args',
		yargs =>
			yargs
				.positional('intcmd', {
					describe:
						'Internal command to run (issue .help in repl or "run help")',
					type: 'string'
				})
				.positional('intargs', {
					describe: 'Internal command argument list (if required)',
					type: 'string'
				})
	)
	.demandCommand(1, 'You need to provide one command to proceed')
	.option('silent', {
		alias: 's',
		describe: 'Supress startup banner and initialization messages',
		type: 'boolean'
	})
	.completion('completion', (current, argv) => {
		if (argv._.includes('run')) {
			return Object.keys(commands).filter(c => runnable(c))
		}
		return ['generate', 'clean', 'init', 'repl', 'run', 'serve']
	})
	.alias('?', 'help')
	.help().argv

// yargs should be done this already :-(
if (
	typeof argv._[0] !== 'string' ||
	!['repl', 'init', 'generate', 'clean', 'serve', 'run'].includes(argv._[0])
) {
	yargs.showHelp()
	console.error('\nInvalid draig command: %s', argv._[0])
	process.exit(0)
}

// Configure apiProject and projectDir based on current dir
if (!argv._.includes('init')) configProject(argv)
