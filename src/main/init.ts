import fs from 'fs'
import path from 'path'
import chalk from 'chalk'
import yaml from 'js-yaml'
import { Context } from 'node:vm'
import clearModule from 'clear-module'

import { templateList, setPrompt } from './util'
import { apiValue } from '../actions/common'
import commands from '../commands'

// Initialization
// TODO: Fix REPLServer hacks to allow to use here the REPLServer legal @type
/* eslint-disable @typescript-eslint/no-explicit-any */
export function initRepl(repl: any) {
	const ctx = repl.context
	const oldCompleter = repl.completer
	ctx.repl = repl
	repl.writer.options.depth = null
	repl.completer = (
		line: string,
		callback: (err?: null | Error, result?: [string[], string]) => void
	): void => {
		let comp = [],
			last: string,
			rest

		if (line.startsWith('.newop ')) {
			comp = templateList(ctx.argv, 'op')
			last = line.split(' ')[1]
		} else if (line.startsWith('.newsch ')) {
			comp = templateList(ctx.argv, 'schema')
			last = line.split(' ')[1]
		} else if (line.startsWith('.addprop ')) {
			comp = Object.keys(ctx.api.components.schemas)
			last = line.split(' ')[1]
		} else if (
			line.startsWith('.edit ') ||
			line.startsWith('.rm ') ||
			line.startsWith('.cd ') ||
			line.startsWith('.ls ') ||
			line.startsWith('.print ') ||
			line.startsWith('.addelem ') ||
			line.startsWith('.annotate ')
		) {
			const args = line
				.split(' ')
				.slice(1)
				.filter(e => e !== '')
			last = line.endsWith(' ') ? '' : args[args.length - 1]
			rest = line.endsWith(' ') ? args : args.slice(0, args.length - 1)
			const [value] = apiValue(ctx, rest)
			if (typeof value !== 'undefined') comp = Object.keys(value)
		} else if (
			line.startsWith('.migrate up ') ||
			line.startsWith('.migrate down ')
		) {
			comp = fs.readdirSync(path.resolve(ctx.argv.projectDir, 'migrations'))
			last = line.split(' ')[2]
		} else if (line.startsWith('.migrate ')) {
			comp = ['up', 'down']
			last = line.split(' ')[1]
		} else if (line.startsWith('.seed ')) {
			comp = fs.readdirSync(path.resolve(ctx.argv.projectDir, 'seeds'))
			last = line.split(' ')[1]
		} else if (line.startsWith('.desc ')) {
			comp = Object.keys(ctx.model).map(e => ctx.model[e].tableName)
			last = line.split(' ')[1]
		} else if (line.startsWith('.dbreset ')) {
			comp = ['destroy']
			last = line.split(' ')[1]
		} else return oldCompleter(line, callback)
		const hits: string[] = comp.filter(c => c && c.startsWith(last))
		const ret: [string[], string] = [
			last !== undefined ? hits : comp,
			last || ''
		]
		callback(null, ret)
	}
}

export async function initCtx(ctx: Context) {
	if (!ctx.argv.silent) console.log('Context (re)initialized')
	// Read api definition (again)
	if (ctx.argv && ctx.argv.api) {
		ctx.apiPath = []
		ctx.api = yaml.safeLoad(fs.readFileSync(ctx.argv.api))
		setPrompt(ctx)
		if (!ctx.argv.silent)
			console.log(chalk`> API from {italic ${ctx.argv.api}} (re)loaded`)
		for (const [n, c] of Object.entries(commands)) ctx.repl.defineCommand(n, c)
	} else if (!ctx.argv.silent) console.log(`No API loaded`)
	const projPath = path.resolve(ctx.argv.projectDir)
	if (fs.existsSync(path.join(projPath, 'utils', 'orm.js'))) {
		try {
			// May have been changed after an schema uddate
			clearModule(path.join(projPath, 'models.js'))
			const orm = await import(path.join(projPath, 'utils', 'orm.js'))
			ctx.knex = orm.default.knex()
			const c = ctx.knex.client.config
			const cc = c.connection
			if (!ctx.argv.silent)
				console.log(
					chalk`> Connection with {dim ${c.client}} to {dim ${
						cc.database
							? cc.database
							: path.relative(process.cwd(), cc.filename)
					}}`
				)
			if (c.connection.host && c.connection.user && !ctx.argv.silent)
				console.log(chalk`> at {dim ${cc.host}} as {dim ${cc.user}} user`)
			if (c.debug && !ctx.argv.silent)
				console.log('  DB debug is: ' + c.debug ? c.debug : false)
			if (fs.existsSync(path.join(projPath, 'models.js'))) {
				ctx.model = require(path.join(projPath, 'models.js'))
				if (!ctx.argv.silent)
					console.log(
						chalk`> {dim ${Object.keys(ctx.model).length - 2}} models loaded\n`
					)
			}
		} catch (e) {
			console.error(
				'%s\nPlease check your API files for typos and required deps' +
					' - Did you forget to make "yarn install" ( or .install)?',
				e
			)
		}
	} else if (!ctx.argv.silent)
		console.log('> No orm.js file found - Is the project generated?')
}
