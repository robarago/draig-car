import chalk from 'chalk'
import { Docker } from 'node-docker-api'
import { Container } from 'node-docker-api/lib/container'
import { Readable, Writable, Stream } from 'stream'

import { loadConfig } from './util'
import contcfg from '../cfg/container'

const docker = new Docker(undefined)

interface ImageObj {
	State: string
}
interface ImageStatusObj {
	State: { Status: string }
}

async function findImage(image: string): Promise<boolean> {
	const images = await docker.image.list({
		filters: `{ "reference": ["${image}"] }`
	})
	return !!images.length
}

async function findContainer(cont: string): Promise<Container | undefined> {
	const conts = await docker.container.list({
		all: true,
		filters: `{ "name": ["${cont}"] }`
	})
	return conts[0]
}

export async function runContainer(client: string, destroy: boolean) {
	const ccfg = contcfg(loadConfig())
	const dbc = 'draigdb-' + client
	const andContainer = destroy ? 'and container' : ''
	let mycont = await findContainer(dbc)
	if (mycont) {
		console.warn(
			chalk`Container {green ${dbc}} already exists - ` +
				`Existing user's schema/DB ${andContainer}will be destroyed`
		)
		if (destroy) {
			await mycont.delete({ force: true })
		} else return mycont
	}
	console.info(
		chalk`Creating container {green ${dbc}} with image` +
			chalk` {green ${ccfg[client].img}}`
	)
	mycont = await docker.container.create({
		name: dbc,
		env: ccfg[client].env,
		Image: ccfg[client].img,
		ExposedPorts: {
			[`${ccfg[client].port}/tcp`]: {}
		},
		HostConfig: {
			NetworkMode: 'default',
			PortBindings: {
				[`${ccfg[client].port}/tcp`]: [{ HostPort: `${ccfg[client].port}` }]
			}
		},
		Ulimits: [{ Name: 'core', Hard: 0, Soft: 0 }]
	})
	await mycont.start()
	const imgStat = (await mycont.status()).data as ImageStatusObj
	const state = imgStat.State.Status
	if (state !== 'running') {
		console.error(
			chalk`{red ERROR}: Container {green ${dbc}} did not started - ` +
				chalk`State: {green ${state}}`
		)
	} else {
		console.log(
			chalk`Waiting {green ${ccfg[client].t}} seconds for the DB to start up`
		)
		await new Promise(r => setTimeout(r, 1000 * ccfg[client].t))
		return mycont
	}
}

export async function execInContainer(mycont: Container, client: string) {
	const ccfg = contcfg(loadConfig())
	const ex = await mycont.exec.create({
		Tty: false,
		AttachStdin: true,
		AttachStdout: true,
		AttachStderr: true,
		Cmd: ccfg[client].cmd
	})
	console.log(chalk`(Re)creating {green client} schema/db...`)
	const stream = (await ex.start({
		hijack: true,
		stdin: true
	})) as Writable
	const readable = Readable.from(ccfg[client].lines.map(l => l + '\n'))
	docker.modem.demuxStream(stream, process.stdout, process.stderr)
	readable.pipe(stream)
	return new Promise(resolve => {
		stream.on('end', resolve)
	})
}

export async function contAction(
	client: string,
	reqState: string,
	actionLabel: string
) {
	const dbc = 'draigdb-' + client
	try {
		const mycont = await findContainer(dbc)
		if (mycont) {
			const state = (mycont.data as ImageObj).State
			if (state !== reqState) {
				console.info(chalk`${actionLabel} ${state} container {green ${dbc}}`)
				const result =
					reqState === 'running'
						? await mycont.start().then(c => c.status())
						: await mycont.stop().then(c => c.status())
				const status = (result.data as ImageStatusObj).State.Status
				console.log(chalk`New status: {green ${status}}`)
			} else {
				console.warn(chalk`Container {green ${dbc}} yet in state ${state}`)
			}
		} else {
			console.error(
				chalk`{red ERROR}: Container {green ${dbc}} does not exist - ` +
					chalk`Try {green dbreset [destroy]} command`
			)
		}
	} catch (e) {
		console.error(
			chalk`{red ERROR}: ${e.message} - ` +
				'Please, make sure that docker service is running'
		)
	}
}

export async function pullImage(client: string) {
	const ccfg = contcfg(loadConfig())
	const image = ccfg[client].img
	if (await findImage(image)) return
	console.warn(
		chalk`Image {green ${image}} for {green ${client}} not found -` +
			' Please, wait while the image is downloaded'
	)
	const stream = (await docker.image.create({}, { fromImage: image })) as Stream
	await new Promise((resolve, reject) => {
		docker.modem.followProgress(
			stream,
			err => {
				process.stdout.write('\n')
				if (err) {
					console.error(chalk`{red ERROR}: ${image} failed - ${err}`)
					reject(false)
				} else {
					console.log(chalk`Image {green ${image}} pulled OK`)
					resolve(true)
				}
			},
			event => {
				let outputMessage = event.status
				if (event.id) outputMessage += ': ' + event.id
				if (event.progress) outputMessage += ' ' + event.progress
				process.stdout.write(outputMessage + ' \r')
			}
		)
	})
}
