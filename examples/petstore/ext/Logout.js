const logger = require('../petstore/logger')

module.exports.Logout = class {
	constructor() {
		logger.info('Logout service created')
	}
	invoke(reqParams) {
		logger.info('Logout service invoked')
		return 'Logout succeded'
	}
}
