const fs = require('fs')
const logger = require('../petstore/logger')

module.exports.UploadService = class {
	constructor() {
		logger.info('Upload service created')
	}
	invoke(reqParams) {
		const f = reqParams.body.file
		logger.info(`Uploading '${f.originalname}' with size: ${f.size} bytes`)
		fs.writeFileSync(f.originalname, f.buffer)
		return { code:0, type:'success', message:'Upload succeded' }
	}
}
