// To see debug messages run DEBUG="oauth:AuthHandler" node server.js
const debug = require('debug')('oauth:AuthHandler')

class AuthHandler {
	static apiKey(req, scopes, schema) {
		// Only called when api_key is present in headers
		const api_key = req.headers['api_key']
		const authorized = api_key === 'special-key'
		debug(`In apiKey api_key="${api_key}", authorized=${authorized}`)
		return authorized
	}
}

module.exports = AuthHandler
