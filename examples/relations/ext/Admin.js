const logger = require('../chat/logger')
const knex = require('../chat/utils/orm').knex()

module.exports.Admin = class {
	constructor() {
		logger.info('Admin service instantiated')
	}

	invoke(reqParams) {
		const objname = decodeURI(reqParams.object)
		logger.info('Looking for items of type "' + objname + '"')
		return knex(objname)
	}
}
