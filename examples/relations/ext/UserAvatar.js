const logger = require('../chat/logger')
const knex = require('../chat/utils/orm').knex()

module.exports.UserAvatar = class {
	constructor(superagent) {
		this.request = superagent
		logger.info('UserAvatar service instantiated')
	}

	async invoke(reqParams) {
		logger.info('Getting robo-avatar for user ' + reqParams.name)
		// Ensure that user exists
		const user = await knex('users').where('username', reqParams.name).first()
		if (user) {
			const name = encodeURIComponent(reqParams.name)
			// Exceptions in call to ext service are catched by the controller
			let res = await this.request.get(`https://robohash.org/${name}`)
			// Body is a png image (Buffer): hash to show as img src:base64
			return { img: Buffer.from(res.body).toString('base64') }
		}
		return Service.rejectResponse(
			`User '${reqParams.name}' not found - Check spelling - Looking in 'users' table with username='${reqParams.name}'`,
			404
		)
	}
}
