# DRAIG (CLI-and-REPL)

This is the Command Line Interpreter and Read-Eval-Print Loop for [DRAIG](https://gitlab.com/robarago/draig.git)
(Database REST-API Interactive Generator).

## Requisites

You need a few tools to develop, generate and test your microservices:

- Operating System: **GNU/Linux** or **Windows** (working with some caveats).
  **macOS** should work but it's not tested and it would need some more work
- NodeJS v14 or higher
- JDK/8 or higher to be able to generate code
- [DRAIG](https://gitlab.com/robarago/draig.git) (required). You can also use
  [openapi-codegen](https://github.com/OpenAPITools/openapi-generator) or
  swagger-codegen (not tested) but some tweaks will be needed to the configuration files.
- Working installation of docker (GNU/Linux, macOS) or Docker Desktop (Windows)
  is also required if you want to use MySQL, PostgreSQL or OracleDB as the DB
  backend (sqlite3 does not require any docker installation).

## Installation

Use `npm -g install draig-car`

For development work on `draig-car` itself, clone the GitLab repo and do
an `npm -g install` or `yarn -g install` to have a local working copy.

## Getting started

Choose one of two options:

1. Start a project with an existing OpenAPI3 contract. You will need to create
   the configuration file `draig.yaml`.

2. Create a project without an OpenAPI3 contract, that will be newly created with
   matching configuration.

For both options, you should use the following cli command:

```
> mkdir <project> && cd $_ # Optional
> draig init <project> <port> <mysql|sqlite3|pg|oracledb> <dbname>
```

If the API definition `<project>.yaml` already exists it will not be overwritten
and it will be used as the generation source for the microservice.

The config file (and the `<project>.yaml` file, if it didn't existed) will be
created in the directory where the command is issued. You should create your own
project directory (if required).

Now you can tweak or complete your API definition using any YAML editor. Also,
you can use the draig REPL to inspect and complete the API definition, with is
automatically saved after each change.

## Initial DB creation

If you are using `mysql, oracledb` or `pg` database clients, `draig-car` need
to create a proper container for the database and to create the database user
with the required privileges. To do so, issue the following command:

```
> draig run dbreset
> draig run dbstart
```

Note: if you're using `sqlite3` database client, then the previous step is not
applicable, don't run it.

If the image is not previously downloaded, it will take a little longer.
Please, be patient.

If you switch between projects (same database client but different databases)
you'll need to destroy the DB container so that it's recreated - Use this:

```
> draig run dbreset destroy
```

Or if you find any other problem with the `draigdb-xxx` container.

## Code generation

Just sit on your `<project>` dir so you can generate the project code using
the following command:

```
> draig generate
```

This command not only generates the code, but also creates and populates the
database with a generated seed based in
[faker](https://github.com/marak/Faker.js) (if `seedGen.__useFaker` is enabled).

## Running the code

If the generation is successfull and the DB is created and populated, the
API could be served inmediatelly.

```
> draig serve
```
