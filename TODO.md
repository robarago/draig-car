# Next patch: fixes and improvements

+ `faker` seeds: work in progress (faker extensions?, customization?)
- Petstore: revisite awful endpoints
- Custom base model in models to provide hacks to Ids and some param normalization

# Next minor: features

- application/xml media type support
- Custom services: customization via x-draig-nogenerate and x-draig-ioc-service:
  should require `package.json` patching for external requires auto merging - Use
  (Dependency injection)[https://github.com/zazoomauro/node-dependency-injection]
- OAuth2: strong validations of the access code and refresh tests
- OAuth2: tested client credentials
- OAuth2: tested refresh_token grant (not supported by Swagger-UI?)
- OAuth2: use JWT for tokens - Is this really useful? NOOO
- OAuth2: add user/password validation!
- OAuth2: working scopes
- Api_key security: custom code testing and usability
- OAuth2: use Knex database as backend for OAuth model instead of memory! - only User model required

* Compatibility with API Managers (statistics, audit, transaction, tracing)...
* Beta testing program
* New command `newsch`: generate CRUD operations automatically after schema creation
* Test, regresion tests, unit tests, integrations tests... tests, tests, tests.
* UUIDs: java and seed generator compatibility with GUUIDs
* Docs: video tutorials, startup guide, template guide, x-draig docs
* POST generalization to array of objects?
* Introspection!: inverse engineering of DB schema to OAS3 schemas w/relations
* UUID generation instead of sequence
* Better migrations (i.e. django-like)
* Headers in response? (i.e. X-Rate-Limit, X-Expires-After, etc.)
* OAuth2: test password, client_credentials
* OAuth2: testing with external servers - oauthServer generator switch also
* OAuth2: generation of code without OAuth2 server - Minimize dependencies?

# Done (2.X.X)

- Recodification in TypeScript
- OAuth2 / API Auth: in progress

# Next major (3.X.X)

- Java support ?
- MongoDB + Mongoose / KV Store / ODM ?
- Microservice patterns ?
